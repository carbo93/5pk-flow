<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/test1', [
    'uses' => 'TestController@step1'
]);
Route::post('/test1', [
    'uses' => 'TestController@check1'
]);
Route::get('/test1NoPost', [
    'uses' => 'TestController@check1'
]);
Route::get('/test2', [
    'uses' => 'TestController@step2'
]);
Route::post('/test2', [
    'uses' => 'TestController@check2'
]);
Route::get('/test2NoPost', [
    'uses' => 'TestController@check2'
]);