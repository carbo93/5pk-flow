<?php 
namespace App\Poker\Repositories;
use App\Poker\PokerRepository;

class Pk5Repository extends PokerRepository {
    /*
Name	Chinese Name	Rate	代號
Royal Flush	同花大順	800	10
Five of a kind	五梅	400	11
Straight Flush	同花順	100	9
Four of a kind	鐵支	40	8
Full House	葫蘆	10	7
Flush	同花	7	6
Straight	順子	5	5
Three of a kind	三條	3	4
Two Pairs	兩對	2	3
One Pair	一對(J以上)	1	2*/
    protected $rate = array(
        0=>0,
        1=>1,
        2=>2,
        3=>3,
        4=>5,
        5=>7,
        6=>10,
        7=>40,
        8=>100,
        9=>400,
        10=>800,
        
    );
    function __construct(){
        parent::__construct();
        $temp = array();
        foreach ($this->ranks as $v){
            if($v == "A"){
                $temp[10]=$v;
            }else if($v == "B"){
                $temp[11]=$v;
            }else if($v == "D"){
                $temp[12]=$v;
            }else if($v == "E"){
                $temp[13]=$v;
            }else{
                $temp[$v]=$v;
            }
        }
        $temp[99] = "F";
        ksort($temp);
        $this->ranks = $temp;
        $temp2 = array();
        //'S','H','D','C'
        foreach ($this->suit as $v){
            if($v == "A"){
                $temp2[4]=$v;
            }else if($v == "B"){
                $temp2[3]=$v;
            }else if($v == "C"){
                $temp2[2]=$v;
            }else if($v == "D"){
                $temp2[1]=$v;
            }
        }
        $temp2[99] = "D";
        $this->suit = $temp2;
        //5pk加入鬼牌
        $this->nowLeftCards[] = array(99,99);
        shuffle($this->nowLeftCards);
    }

    
    public function cards2array($cards){
        $out = array();
        foreach($cards as $k=>$data){
            $suit = $data[0];
            $ranks = $data[1];
            if(isset($data[2])){
                $ranks = $data[1].$data[2];
            }
            $out[$k] = array(array_search($ranks, $this->ranks) , array_search($suit, $this->suit));
        }
        return $out;
    }
    public function array2cards($array){
        $out = array();
        foreach($array as $k=>$data){
            $suit = $data[1];
            $ranks = $data[0];
            $out[$k] = $this->suit[$suit].$this->ranks[$ranks];
        }
        return $out;
    }
    
    
    
    
    public function randomRank($j=5,$palyers = array('A','B','C','D'))
    {
        $all = $this->array2cards($this->nowLeftCards);
        $out = array();
        foreach($palyers as $index => $name){
            for($i=0;$i<$j;$i++){
                $out[$name][] = array_pop($all);
            }
        }
        $out['discard'] = $all;
        return $out;
    }
    
    public function step1(){
        //ANDY is 帥哥
        $db = new Pk5();
        $start = $this->randomRank(5,array('A'));
        $db->A = json_encode($start['A']);
        $db->parent_id = 0;
        $db->discard = json_encode($start['discard']);
        $db->save();
        $start['id'] = $db->id;
        return $start;
    }
    public function step2($room,$use = array()){
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$room))->get();
        if(empty($res->toArray())){
            return false;
        }else{
            $res2 = $db->query()->where(array('parent_id'=>$room))->orderBy('id', 'desc')->get();
            if(empty($res2->toArray())){
                $all = $res->toArray();
                $j = 5;
                $A = json_decode($all[0]['A'],true);
                $start = array();
                $start['A'] = array();
                $start['deal'] = json_decode($all[0]['deal'],true);
                $start['discard'] = json_decode($all[0]['discard'],true);
                
                if(is_array($use) && !empty($use)){
                    foreach($use as $u){
                        //do something
                        $j--;
                        $start["A"][]=$A[$u];
                        unset($A[$u]);
                    }
                }
                
                
                for($i=0;$i<$j;$i++){
                    $start["A"][] = array_pop($start['discard']);
                }
                foreach($A as $v){
                    $start['deal'][]=$v;
                }
                
                $db->A = json_encode($start['A']);
                $db->parent_id = $all[0]['id'];
                $db->deal = json_encode($start['deal']);
                $db->discard = json_encode($start['discard']);
                $db->save();
                $start['id'] = $db->id;
                return $start;
            }else{
                return false;
            }
            
        }
    }
    public function check1($room){
        $out = array();
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$room))->get();
        if(empty($res->toArray())){
            return false;
        }
        $res2 = $db->query()->where(array('parent_id'=>$room))->orderBy('id', 'desc')->get();
        if(empty($res2->toArray())){
            $data = $res->toArray();
        }else{
            $data = $res2->toArray();
        }
        $A = $data[0]['A'];      
        $discard = $data[0]['discard'];
       
        $A = json_decode($A,true);
        $discard = json_decode($discard,true);
        
        $out['A'] = $A;
        $out['discard'] = $discard;
        
        
        $temp = array();
        $recyc = array();
        
        if (false !== $key = array_search("DF", $A)) {
            unset($A[$key]);
            //鬼牌啟動
            foreach($discard as $kk=>$vv){
                $temp[$kk]=$A;
                $temp[$kk][$key] = $vv;
            }
        }else{
            $temp[] = $A;
        }
        foreach($temp as $kkk=>$vvv){
            $vvv = $this->cards2array($vvv);
            $res = $this->judge($vvv);
            $recyc[$kkk] = $res;
        }
        $recycCount = array();
        foreach ($recyc as $key=>$value){
            if(isset($recycCount[$value])){
                $recycCount[$value] = $recycCount[$value]+1;
            }else{
                $recycCount[$value] = 1;
            }
        }
        $max  = max($recyc);  // 7
        $maxkey = array_search($max, $recyc);
        $out['judge'] = $max;
        if(isset($recycCount[8]) && $recycCount[8] == 48){
            $out['judge'] = 9;
        }
        $out['result'] = $temp[$maxkey];
        $out['resultArray'] = $this->cards2array($temp[$maxkey]);
        if($max == 1){
            $temp222 = array();
            foreach($out['resultArray'] as $vvvvv){
                if(isset($temp222[$vvvvv[0]])){
                    $temp222[$vvvvv[0]] = $temp222[$vvvvv[0]]+1;
                }else{
                    $temp222[$vvvvv[0]] = 1;
                }
            }
            for($jj=2;$jj<=10;$jj++){
                if(isset($temp222[$jj]) && $temp222[$jj] == 2){
                    $out['judge'] = 0;
                }
            }
        }
        $out['rate']= $this->rate[$out['judge']];
        return $out;
    }
}