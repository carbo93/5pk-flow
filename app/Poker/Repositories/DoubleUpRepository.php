<?php 
namespace App\Poker\Repositories;
use App\Poker\PokerRepository;

class DoubleUpRepository extends PokerRepository {
    
    function __construct(){
        $temp = array();
        foreach ($this->ranks as $v){
            if($v == "A"){
                $temp[14]=$v;
            }else if($v == "J"){
                $temp[11]=$v;
            }else if($v == "Q"){
                $temp[12]=$v;
            }else if($v == "K"){
                $temp[13]=$v;
            }else{
                $temp[$v]=$v;
            }
        }
        ksort($temp);
        $this->ranks = $temp;
        $temp2 = array();
        //'S','H','D','C'
        foreach ($this->suit as $v){
            if($v == "S"){
                $temp2[4]=$v;
            }else if($v == "H"){
                $temp2[3]=$v;
            }else if($v == "D"){
                $temp2[2]=$v;
            }else if($v == "C"){
                $temp2[1]=$v;
            }
        }
        $this->suit = $temp2;
    }
    
    public function step1($id = 0){
        $start = $this->randomRank(5,array('all'));
        $card = $start['all'];
        $B = $start['all'][0];
        unset($start['all'][0]);
        $input = array();
        $input['A'] = "";
        $input['B'] = json_encode($B);
        $input['card'] = json_encode($card);
        if($id == 0){
            $id = $this->insert(0, $input['A'], $input['B'], $input['card']);
        }else{
            $id = $this->insert($id, $input['A'], $input['B'], $input['card']);
        }
        return $id;
    }
    private function insert($id, $A ,$B ,$card){
        if($B !="" && $card !=""){
            $db = new DoubleUp();
            $db->A = $A;
            $db->B = $B;
            $db->card = $card;
            $db->parent_id = $id;
            $db->status = 0;
            $db->save();
            return $db->id;
        }
        return false;
        
    }
    
    public function step2($id,$status){
        $db = new DoubleUp();
        $res = $db->query()->where(array('id'=>$id))->get();
        if(empty($res->toArray()) || !isset($res->toArray()[0])){
            return false;
        }
        $data = $res->toArray()[0];
        if(intval($data['status']) == 0){
            $res = $db->query()->where(array('id'=>$id))->update(array('status'=>$status));
            if($res == 1){
                //if($data['A'] >= $data['B']){
                if(true){
                    $res = $db->query()->where(array('parent_id'=>$id))->get();
                    if(empty($res->toArray()) || !isset($res->toArray()[0])){
                        return false;
                    }
                    $data = $res->toArray()[0];
                    return $data['id'];
                }
            }
        }
        return false;
        
        
    }
    
}