<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Game5pk;
use App\Repositories\GameLogin;
use App\Repositories\Order;
use App\Repositories\Wallet;
use App\Repositories\Table;
class Swoole extends Command
{
    protected $game5pk;
    protected $gameLogin;
    protected $wallet;
    protected $order;
    protected $table;
    protected $signature = 'swoole {action?}';
    protected $description = 'swoole websocket server';
    protected $ws;
    
    public function __construct(
        Game5pk $game5pk,
        GameLogin $gameLogin,
        Order $order,
        Wallet $wallet,
        Table $table
    )
    {
        parent::__construct();
        $this->game5pk = $game5pk;
        $this->gameLogin = $gameLogin;
        $this->order = $order;
        $this->wallet = $wallet;
        $this->table = $table;
    }
    
    public function handle()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'start':
                $this->start();
                break;
            case 'gameMaintainNotice':
                $this->gameMaintainNotice();
                break;
            case 'gameWebsocketClose':
                $this->gameWebsocketClose();
                break;
            default:
                break;
        }
    }
    public function gameMaintainNotice()
    {
        \Ratchet\Client\connect('ws://'.'127.0.0.1'.':'.env('WEBSOCKET_PORT'))->then(function($conn) {
            $conn->on('message', function($msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });
                $temp =array('cmd'=>env('APP_KEY')."_Notice");
                $temp = base64_encode(json_encode($temp));
                $conn->send($temp);
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
    }

    public function gameWebsocketClose(){
        \Ratchet\Client\connect('ws://'.'127.0.0.1'.':'.env('WEBSOCKET_PORT'))->then(function($conn) {
            $conn->on('message', function($msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });
                $temp =array('cmd'=>env('APP_KEY')."_Close");
                $temp = base64_encode(json_encode($temp));
                $conn->send($temp);
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });

    }


    public function start(){


            $this->initWebSocketServer();
            //監聽WebSocket連結打開事件
            $this->ws->on('open', function ($ws, $request) {

                $ws->user_c[$request->fd] = $request->fd;
                
                $this->startRound($ws, $request);
            /*
                $msg = array(
                    'table'=>1,
                    'user_id'=>$request->fd,
                    'type'=>1,
                );
                $this->broadcast($ws, $request, $msg);
            */
            });

            //監聽WebSocket消息事件
            $this->ws->on('message', function ($ws, $frame) {
                try{
                    
                    //$msg =  'from'.$frame->fd.":{$frame->data}\n";
                    $msg = json_decode(base64_decode($frame->data));
                    //var_dump($msg);
                    //$error_msg = $ws->game5pk->errorMessage($data);
                    $not_allow = array(
                        8001101,
                        8001199,
                        env('APP_KEY')."_Notice",
                        env('APP_KEY')."_Close",
                        env('APP_KEY')."_Winner"
                    );
                    if(!in_array($msg->cmd, $not_allow)){
                        $this->login_check($ws, $frame); 
                    }

                    switch($msg->cmd){
                        case env('APP_KEY')."_Notice": 
                            $this->maintainNotice($ws, $frame); 
                        break;
                        case env('APP_KEY')."_Close": 
                            $this->websocketClose($ws, $frame); 
                        break;
                        case env('APP_KEY')."_Winner": 
                            //大獎廣播
                            foreach($msg->data as $m){
                                $msg_array = array(
                                    'table'=>isset($m->table_id)?$m->table_id:0,
                                    'user_name'=>isset($m->user_name)?$m->user_name:'',
                                    'type'=>isset($m->type)?$m->type:0
                                );
                                $this->broadcast($ws, $frame , $msg_array);
                            }
                            
                        break;
                        case 8001101: //gameLogin(c2s)
                            $this->gameLoginAck($ws, $frame); 
                        break;
                        case 8001102: //bet(c2s) create order and 1's orderitem
                            $this->betAck($ws, $frame); 
                        break;
                        case 8001103: //KeepCard(c2s) payout 1's orderitem
                            $this->keepCardAck($ws, $frame); 
                        break;
                        case 8001104: //JoinSubGame(c2s)  create 2's orderitem
                            
                            if($msg->data->join){
                                //比被大於最大值
                                //if($ws->win_amount[$frame->fd]*2<=$ws->win_amount_max){
                                    $this->joinSubGameAck($ws, $frame);
                                //}else{
                                //    $this->finishGameRound($ws, $frame);
                                //    $this->settlement($ws, $frame); 
                                //}
                            }else{
                                //$this->joinSubGameAck($ws, $frame); break;
                                $this->finishGameRound($ws, $frame);
                                $this->settlement($ws, $frame); 
                            }
                        break;
                        case 8001105: //SelectSubGameCard(c2s) payout 2's orderitem
                            $this->selectSubGameCardAck($ws, $frame); 
                        break;
                        case 8001106: //GameLogout(c2s) //final payout order
                            $this->gameLogoutAck($ws, $frame); 
                        break;

                        case 8001107: //JoinTable
                            echo "TABLE_NUM::"; 
                            echo $msg->data->table_num."\n\n";
                            $table_num = $msg->data->table_num;
                            $this->joinTable($ws, $frame, $table_num); 
                        break;
                        case 8001108: //TableList
                            $this->tableList($ws, $frame); 
                        break;
                        case 8001109: //leaveTable
                            $this->leaveTable($ws, $frame); 
                        break;

                        case 8001121: //WalletTransferAck(c2s)
                            $this->walletTransferAck($ws, $frame); 
                        break;
                        case 8001122: //WalletBalaceAck(c2s)
                            $this->walletBalaceAck($ws, $frame); 
                        break;

                        case 8001199: //PingPongAck(c2s)
                            $this->pingPongAck($ws, $frame); 
                        break;

                    }
                }
                catch(Exception $e)
                {
                    echo $e;
                }

            });

            //監聽WebSocket連結關閉事件
            $this->ws->on('close', function ($ws, $fd) {
                try{
                    $frame_array = array(
                        "fd"=>$fd
                    );
                    $frame = (object) $frame_array ;
                    
                    $not_allow = array(
                        0,
                        env('APP_KEY')."_Notice",
                        env('APP_KEY')."_Close",
                        env('APP_KEY')."_Winner"
                    );
                    if(in_array($ws->operation_code, $not_allow)){
                        echo "我叫eason";
                        echo "\n";
                    }else{
                        $this->leaveTable($ws, $frame); 
                        
                        
                        switch($ws->operation_code){
                            case 8001102:
                                $this->keepCardAck($ws, $frame);
                                $this->finishGameRound($ws, $frame);
                                $this->settlement($ws, $frame);
                                break;
                                
                            case 8001103:
                                $this->finishGameRound($ws, $frame);
                                $this->settlement($ws, $frame);
                                break;
                                
                            case 8001104:
                                $this->selectSubGameCardAck($ws, $frame);
                                $this->finishGameRound($ws, $frame);
                                $this->settlement($ws, $frame);
                                break;
                        }
                        /*
                         if($ws->operation_code!=8001014){
                         
                         echo "自動關單";
                         echo $ws->operation_code."\n";
                         echo $ws->login_token[$fd]."\n";
                         echo $ws->game_id."\n";
                         echo $ws->game_token[$fd]."\n";
                         
                         
                         $data = array(
                         "token"=> $ws->login_token[$fd],
                         "game_id"=> $ws->game_id,
                         "game_token"=> $ws->game_token[$fd],
                         );
                         $gmae_login_ack_msg = $ws->gameLogin->gameLogoutAck($data);
                         }
                         */
                        
                        echo "刪除斷開的連結{".$fd."}";
                        unset($ws->user_c[$fd]);
                    }
                }
                catch(Exception $e)
                {
                    echo $e;
                }
                
            });

            $this->ws->on('Receive', function ($ws, $fd) {
                try
                {
                }
                catch(Exception $e)
                {
                    echo $e;
                }
            });
            $this->ws->start();
        
    }

    private function initWebSocketServer(){
        

            $this->ws = new \swoole_websocket_server(env('WEBSOCKET_IP'), env('WEBSOCKET_PORT'));
            $this->ws->set(array(
                'worker_num'     => env('WORKER_NUM'),
                'ipc_mode'       => env('IPC_MODE'),
                'max_request'    => env('MAX_REQUEST'),  
                'max_connection' => env('MAX_CONNECTION'),
                'dispatch_mode'  => env('DISPATCH_MODE'),
                'daemonize'      => env('DAEMONIZE'),      //設置程序進入後台作為守護進程運行
                'heartbeat_check_interval' => env('HEARTBEAT_CHECK_INTERVAL'),
                'heartbeat_idle_time' => env('HEARTBEAT_IDLE_TIME'),
                'timeout' => env("TIMEOUT"),
                //'log_file' => '/var/logs/5pk-flow.log',
                //'socket_buffer_size' => 128 * 1024 *1024, //只能為數字
            ));
            $this->ws->user_c = [];   //儲存每一個進來的ws
            $this->ws->bet_amount = [];   //BetAmount
            $this->ws->win_amount = [];   //winAmount
            $this->ws->game_token = [];   //game_token
            $this->ws->login_token = [];  //login_token
            $this->ws->order_id = [];      //order_id
            $this->ws->order_item_id = []; //order_item_id
            $this->ws->user_id = [];      //user_id
            $this->ws->user_name = [];    //user_name
            $this->ws->account_name = []; //account_name
            $this->ws->bet_range = array(
                10, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000
            );
            $this->ws->win_amount_max = 20000 * 100; //最高中獎限制金額
            $this->ws->game_id = env("GAME_ID");
            $this->ws->operation_code = 0;   //現在執行階段
            $this->ws->game5pk = $this->game5pk;
            $this->ws->gameLogin = $this->gameLogin;
            $this->ws->order = $this->order;
            $this->ws->wallet = $this->wallet;
            $this->ws->table = $this->table;
            $this->ws->round = 0;
            $this->ws->table_id = 0;
            //$this->ws->timer_process = array();
            //$this->ws->time = array();
        
    }

    private function login_check($ws, $frame){

        echo "login check";
        $token = isset($ws->login_token[$frame->fd])?$ws->login_token[$frame->fd]:"";
        $game_token = isset($ws->game_token[$frame->fd])?$ws->game_token[$frame->fd]:"";
        $data = array(
            "token"=>  $token,
            "game_id"=> $ws->game_id,
            "game_token"=> $game_token,
            "conn_id"=> $frame->fd,
        );
        $login_check_array = $ws->gameLogin->loginCheck($data);
        //var_dump($login_check_msg);
        //$login_check_array = ($login_check_msg, true);
        echo "login check=>".$login_check_array["status"];
        
        if($login_check_array["status"]==0){
           //重複登入
           //$ws->disconnect($frame->fd,1000, $token."重複登入");
            
           //loginRepeat
           $msg_array = array(
             'cmd' => 8001098,
             'data' => array(),
           );
           $ws->push($frame->fd, $this->output($msg_array));
           return;
        }

    }

    private function startRound($ws, $request){
        //$ws->operation_code = 8001006;
        //儲存各個ws連線ID
        echo "開始連結{".$request->fd."}";
        //設定倒數時間
        /*
        $fd = $request->fd;
        $ws->time[$fd] = env('BET_COUNTDOWN_TIME');
        $ws->timer_process[$fd] = swoole_timer_tick(1000,function() use ($fd, $ws){
            echo "StartRound連線".$fd."倒數".$ws->time[$fd];
            $please_bet_msg = $ws->game5pk->pleaseBet($ws->time[$fd]);
            $ws->push($fd,$this->output($please_bet_msg));
            //echo $ws->please_bet_countdown;
            var_dump($ws->time[$fd]);
            $ws->time[$fd] = $ws->time[$fd]-1;
            if($ws->time[$fd] < 0 ){
                swoole_timer_clear($ws->timer_process[$fd]);
                $ws->timer_process[$fd]=0;
            }

        });
        */
    }
    
    private function joinTable($ws, $frame, $table_id){

        //var_dump($ws->user_id[$frame->fd]);
        $inputData = array(
            "user_id"=>$ws->user_id[$frame->fd],
            "user_name"=>$ws->user_name[$frame->fd],
            "account_name"=>$ws->account_name[$frame->fd],
            'room_id'=>1,
            'table_id'=>$table_id,
            'user_token'=>$ws->login_token[$frame->fd]
        );
        
        $room_ack_msg = $ws->table->inTable($inputData);
        if($room_ack_msg["data"]["status"]){
            $ws->table_id = $table_id;
            echo "ws->table_id1:";
            echo $ws->table_id;
        }

        $ws->push($frame->fd, $this->output($room_ack_msg));
    }

    private function tableList($ws, $frame){
        $table_list_msg = $ws->table->showTable();

        $ws->push($frame->fd, $this->output($table_list_msg));
    }


    private function leaveTable($ws, $frame){
        //echo "leaveTable";
        //var_dump($ws->user_id[$frame->fd]);

        echo "離桌";

        //if(isset($ws->table_id)&&is_numeric($ws->table_id)){
            $inputData = array(
                //"user_id"=>$ws->user_id[$frame->fd],
                //"user_name"=>$ws->user_name[$frame->fd],
                //"account_name"=>$ws->account_name[$frame->fd],
                'room_id'=>1,
                'table_id'=>$ws->table_id
            );

            //echo "ws->table_id2:";
            //echo $ws->table_id;
            //($inputData);
            $room_ack_msg = $ws->table->leaveTable($inputData);
            //var_dump($room_ack_msg);
            $ws->push($frame->fd, $this->output($room_ack_msg));
        //}
    }

    
    private function gameLoginAck($ws, $frame){
        //echo "login_token";
        $ws->operation_code = 8001001;
        $msg = json_decode(base64_decode($frame->data),true);

        $login_token = $msg["data"]["login_token"];
        $ws->login_token[$frame->fd] = $login_token;
        //var_dump($login_token);
        $data = array(
            "token"=> $login_token,
            "game_id"=> $ws->game_id,
            "conn_id"=> $frame->fd,
        );
        //var_dump($data);
        //echo "gameLoginAck";
        $gmae_login_ack_msg = $ws->gameLogin->gameLoginAck($data);
        //var_dump($gmae_login_ack_msg);
        if(isset($gmae_login_ack_msg["data"]["game_token"])){
           $ws->game_token[$frame->fd] = $gmae_login_ack_msg["data"]["game_token"];
           //echo $gmae_login_ack_msg["data"]["user_id"];
           $ws->user_id[$frame->fd] = $gmae_login_ack_msg["data"]["user_id"];
           $ws->user_name[$frame->fd] = $gmae_login_ack_msg["data"]["user_name"];
           $ws->account_name[$frame->fd] = $gmae_login_ack_msg["data"]["account_name"];
           //echo $ws->user_id[$frame->fd];
           //$this->leaveTable($ws, $frame); 
           $ws->push($frame->fd, $this->output($gmae_login_ack_msg));
        }else{
           echo "登入失敗";
        }
    }

    private function gameLogoutAck($ws, $frame){

        $ws->operation_code = 8001014;
        $data = array(
            "token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
        );
        $gmae_login_ack_msg = $ws->gameLogin->gameLogoutAck($data);
        $ws->push($frame->fd,$this->output($gmae_login_ack_msg));

    }

    private function betAck($ws, $frame){
        /*
        if($ws->timer_process[$frame->fd]!=0){
            swoole_timer_clear($ws->timer_process[$frame->fd]);
        }
        */

        $ws->operation_code = 8001102;
        //統計錢初始化pleaseBet
        $ws->bet_amount[$frame->fd] = 0;
        $ws->win_amount[$frame->fd] = 0;

        $msg = json_decode(base64_decode($frame->data));
        
        //BetAck
        $bet_amount = $msg->data->amount;
        $ws->bet_amount[$frame->fd] = $bet_amount;
        if(in_array($bet_amount, $ws->bet_range)){

            //create order and 1's orderitem
            //echo "建立父訂單======start=======";
            $order_data = array(
                "bet_amount" => $bet_amount,
            );
            $return =  $this->createOrder($ws, $frame, $order_data);
            if($return){

                //echo "建立子訂單======start=======";
                $order_item_data = array(
                    "bet_amount" => $bet_amount,
                    "summary" => "下注"
                );
                $return =  $this->createOrderItem($ws, $frame, $order_item_data);

                //返回betAck是否成功
                $bet_ack_msg = $ws->game5pk->betAck();
                $ws->push($frame->fd,$this->output($bet_ack_msg));
                
                //balance
                $data = array(
                    "login_token"=> $ws->login_token[$frame->fd],
                    "game_id"=> $ws->game_id,
                    "game_token"=> $ws->game_token[$frame->fd],
                );
                $wallet_transfer_array = $this->wallet->walletBalaceAck($data);
                $ws->push($frame->fd,$this->output($wallet_transfer_array));

                //Deal
                echo "ws->table_id3:";
                echo $ws->table_id;
                $deal_msg = $ws->game5pk->deal($ws->user_id[$frame->fd], $ws->user_name[$frame->fd], $ws->round, $ws->table_id);
                $ws->push($frame->fd,$this->output($deal_msg));

            }else{
                $bet_ack_msg = $ws->game5pk->betAckError();
                $ws->push($frame->fd, $this->output($bet_ack_msg));
            }

        }else{
            $bet_ack_msg = $ws->game5pk->betAckError();
            $ws->push($frame->fd, $this->output($bet_ack_msg));
        }
         
    }

    private function keepCardAck($ws, $frame){

        $ws->operation_code = 8001103;        
        //KeepCardAck
        if(isset($frame->data)){
            $msg = json_decode(base64_decode($frame->data));
            $positions_array = $msg->data->positions;
        }else{
            $positions_array = array(1);
        }
        $keep_card_ack_msg = $ws->game5pk->keepCardAck($positions_array);
        $ws->push($frame->fd, $this->output($keep_card_ack_msg));


        if($keep_card_ack_msg["data"]["success"]){

            //GameResult  // 返回保留後的牌型
            //payout 1's orderitem (ＣＨＡＳＥ  ＡＰＩ)
            $bet_amount = $ws->bet_amount[$frame->fd];


            $game_result_array = $ws->game5pk->gameResult($bet_amount, $ws->win_amount_max);


            $ws->push($frame->fd, $this->output($game_result_array));
            
            if(isset($game_result_array["data"]["type"])){
                $result_data = json_encode(array(
                    "type"=>"5pk-1",
                    "data"=>array(
                        "result"=> $game_result_array["data"]["cards"],
                        "judge_id"=>$game_result_array["data"]["type"],
                        "judge_name"=>$game_result_array["data"]["name"],
                        "rate" => $game_result_array["data"]["rate"],
                    )
                ));
                //關閉子訂單
                $input_data = array(
                    "rate"=>$game_result_array["data"]["rate"],
                    "win"=> $game_result_array["data"]["win_amount"],
                    "result"=>$result_data 
                );
                $return = $this->payoutOrderItem($ws, $frame, $input_data);
/*
                if($game_result_array["data"]["type"]>=7){
                    //中大獎
                    $msg_array = array(
                        'table'=>$ws->table_id ,
                        'user_id'=>$ws->user_name[$frame->fd],
                        'type'=>$game_result_array["data"]["type"]
                    );
                    $this->broadcast($msg_array);
                }
*/
                //贏得金額大於上限
                if($game_result_array["data"]["win_amount"]>=$ws->win_amount_max){
                    //此局遊戲結束
                    $this->finishGameRound($ws, $frame);
                    //結算
                    $this->settlement($ws, $frame);
                }else{
                    //結果
                    echo "比倍結果";
                    echo $game_result_array["data"]["type"];
                    if($game_result_array["data"]["type"]>0){
                        //如果第一局贏的時候不大於最大上限值 進行比倍 否則結算
                        //if($game_result_array["data"]["win_amount"]*2<=$ws->win_amount_max){
                            $ws->win_amount[$frame->fd] = $game_result_array["data"]["win_amount"];
                            return;
                        //}else{
                            //此局遊戲結束
                            //$this->finishGameRound($ws, $frame);
                            //結算
                            //$this->settlement($ws, $frame);
                        //}
                        
                    }else{
                        // 0 沒得獎
                        //此局遊戲結束
                        $this->finishGameRound($ws, $frame);
                        //結算
                        $this->settlement($ws, $frame);
                    }
                }
            }else{
                echo "GameResult FAIL";
            }
        }else{
            echo "KeepCardAck FAIL";
        }

    }

    private function joinSubGameAck($ws, $frame){

            //echo "建立子訂單======start=======";
            $order_item_data = array(
                "bet_amount" => $ws->win_amount[$frame->fd],
                "summary" => "比倍",
            );
            $return =  $this->createOrderItem($ws, $frame, $order_item_data);

            $ws->order_item_id[$frame->fd] = $return["order_item"]["uuid"];

            //echo "joinSubGameAck建立子訂單: ".$ws->order_item_id[$frame->fd]."\n";

            $ws->operation_code = 8001104; 
            echo "ws->table_id4:";
            echo $ws->table_id;
            $join_subgame_ack_array = $ws->game5pk->joinSubGameAck($ws->user_id[$frame->fd],$ws->user_name[$frame->fd], $this->ws->round, $ws->table_id);
            $ws->push($frame->fd, $this->output($join_subgame_ack_array));
        

    }

    private function selectSubGameCardAck($ws, $frame){
        $ws->operation_code = 8001105;
                    
        //SelectSubGameCardAck
        $select_subgmae_card_ack_array = $ws->game5pk->selectSubGameCardAck();
        $ws->push($frame->fd, $this->output($select_subgmae_card_ack_array));

        $this->subGameResult($ws, $frame);

    }

    private function walletTransferAck($ws, $frame){

        
        $msg = json_decode(base64_decode($frame->data),true);

        $data = array(
            "token" => $ws->login_token[$frame->fd],
            "game_token" => $ws->game_token[$frame->fd],
            "game_id" => $ws->game_id,
            "coin_type" => $msg["data"]["coin_type"],
            "coin_amount" => $msg["data"]["coin_amount"], 
        );

        //var_dump($data);
        
        $wallet_return_msg = $this->wallet->walletTransferAck($data);
        //var_dump($wallet_return_msg);
        $ws->push($frame->fd, $this->output($wallet_return_msg));

    }


    private function subGameResult($ws, $frame){

        $bet_amount = $ws->win_amount[$frame->fd];
        //SubGameResult
        if(isset($frame->data)){
            $msg = json_decode(base64_decode($frame->data),true);
        }else{
            $msg["data"]["position"] = 1;
        }
        //var_dump($msg);

        $subgame_result_array = $ws->game5pk->subGameResult($msg["data"]["position"], $bet_amount, $ws->win_amount_max);
        $ws->win_amount[$frame->fd] = $subgame_result_array["data"]["win_amount"];

        $ws->push($frame->fd, $this->output($subgame_result_array));

        //create order and 1's orderitem
        //echo "關閉子訂單＝＝＝＝＝start=======";
        //echo "subGameResult關閉子訂單: ".$ws->order_item_id[$frame->fd]."\n";
        $result_data = json_encode(array(
            "type"=>"5pk-2",
            "data"=>array(
                "position"=>$msg["data"]["position"],
                "cards"=> $subgame_result_array["data"]["cards"],
                'type' => $subgame_result_array["data"]["type"],
                'rate' => $subgame_result_array["data"]["rate"],
                'bet_amount' => $subgame_result_array["data"]["bet_amount"],
                'win_amount' => $subgame_result_array["data"]["win_amount"]
            )
        ));
        
        if($subgame_result_array["data"]["type"]==3){
            $rate = 2;
        }elseif($subgame_result_array["data"]["type"]==2){
            $rate = 1;
        }else{
            $rate = 0;
        }

        $input_data = array(
            "rate"=> $rate,
            "win"=> $ws->win_amount[$frame->fd],
            "result"=>$result_data 
        );
        $return = $this->payoutOrderItem($ws, $frame, $input_data);


        if($ws->win_amount[$frame->fd]>=$ws->win_amount_max){
            //此局遊戲結束
            $this->finishGameRound($ws, $frame);
            //結算
            $this->settlement($ws, $frame);
        }


        //1 輸 2平手 ３贏
        if($subgame_result_array["data"]["type"]==1){
            //此局遊戲結束
            $this->finishGameRound($ws, $frame);
            //結算
            $this->settlement($ws, $frame);
        }
        

    }

    private function payoutOrderItem($ws, $frame, $input_data){

        echo "關閉子訂單: ".$ws->order_item_id[$frame->fd]."\n";
        $data = array(
            "order_id"=> $ws->order_id[$frame->fd],     //父層訂單ＩＤ
            "item_id"=> $ws->order_item_id[$frame->fd],
            "token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
            "rate"=> $input_data["rate"],
            "win"=> $input_data["win"],
            "remark"=> $ws->game_token[$frame->fd],
            "payout_at"=> date("Y-m-d")."T".date("H:i:s")."Z",
            "result"=> $input_data["result"]
        );

        $return = $this->order->payoutOrderItem($data);
        return $return;

    }

    private function broadcast($ws, $frame, $msg){

        $broadcast_msg = $ws->game5pk->broadcast($msg);

        foreach($ws->connections as $fd){
            //echo $fd." PUSH ";
            $ws->push($fd, $this->output($broadcast_msg));
            
        }        

    }

    private function maintainNotice($ws, $frame){

        $time = 300;
        $broadcast_msg = $ws->game5pk->maintainNotice($time);

        foreach($ws->connections as $fd){
            //echo $fd." PUSH ";
            $ws->push($fd, $this->output($broadcast_msg));
        }        

    }

    private function websocketClose($ws, $frame){

        foreach($ws->connections as $fd){
            //echo $fd." PUSH ";
            echo "close".$frame->fd;
            $ws->disconnect($frame->fd,1000, "維護");

        }       

    }

    private function finishGameRound($ws, $frame){
        
        //echo "關閉父訂單＝＝＝＝＝start=======";
        //關閉父訂單
        $data = array(
            "order_id"=> $ws->order_id[$frame->fd],      //父層訂單ＩＤ
            "token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
            "payout_at"=> date("Y-m-d")."T".date("H:i:s")."Z",
        );
    
        echo "@關閉父訂單".$ws->order_id[$frame->fd]."\n";

        $return = $this->order->payoutOrder($data);

        return $return;

    }

    private function walletBalaceAck($ws, $frame){

        $data = array(
            "login_token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
        );
        $wallet_transfer_array = $ws->wallet->walletBalaceAck($data);

        $ws->push($frame->fd, $this->output($wallet_transfer_array));
    }

    private function settlement($ws, $frame){
        
        //balance
        $data = array(
            "login_token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
        );
        $wallet_transfer_array = $this->wallet->walletBalaceAck($data);
        $ws->push($frame->fd,$this->output($wallet_transfer_array));
        //Settlement
        $settlement_msg = $ws->game5pk->settlement($wallet_transfer_array["data"]["balance"]);
        $ws->push($frame->fd, $this->output($settlement_msg));
        $ws->bet_amount[$frame->fd]=0;
        $ws->win_amount[$frame->fd]=0;

    }

    private function createOrderItem($ws, $frame, $bet_data){
        $data = array(
            "order_id"=> $ws->order_id[$frame->fd],      //父訂單ＩＤ
            "token"=> $ws->login_token[$frame->fd],
            "game_id"=> $ws->game_id,
            "game_token"=> $ws->game_token[$frame->fd],
            "play_code"=> "01",
            "summary"=> $bet_data["summary"],
            "currency_id"=> 99,
            "amount"=> $bet_data["bet_amount"],
            "status"=> 1,
            "created_at"=> date("Y-m-d")."T".date("H:i:s")."Z"
        );
        $return = $this->order->createOrderItem($data);

        $ws->order_item_id[$frame->fd] = $return["order_item"]["uuid"];   //儲存子訂單ID

        echo "建立子訂單: ".$ws->order_item_id[$frame->fd]."\n";

        return $return;
    }

    private function createOrder($ws, $frame, $bet_data){
        $this->ws->round++;
        //echo "建立父訂單======start=======";
        $data = array(
                "token"=> $ws->login_token[$frame->fd],
                "game_id"=> $ws->game_id,
                "game_token"=> $ws->game_token[$frame->fd],
                "game_type"=> "poker",
                "game_round"=> date("Y-m-d")."-".sprintf("%03d", $this->ws->table_id)."-01-".sprintf("%03d", $this->ws->round),
                "currency_id"=> 99,
                "amount"=> $bet_data["bet_amount"],
                "status"=> 1,
                "created_at"=> date("Y-m-d")."T".date("H:i:s")."Z"
            );
        
        //echo $this->ws->round;
        $return = $this->order->createOrder($data);
        if(isset($return["order"]["uuid"])){
            $ws->order_id[$frame->fd] = $return["order"]["uuid"];   //儲存父訂單ID
            echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
            echo "@建立父訂單".$ws->order_id[$frame->fd]."\n";
            return $return;
        }else{
            echo "@建立父訂單失敗\n";
            return false;
        }
    }

    private function pingPongAck($ws, $frame){
        $msg_array = array(
            'cmd' => 8001099,
            'data' => array(
                'msg' => "pong"
            )
        ); 
        $ws->push($frame->fd, $this->output($msg_array));
    }


    private function output($msg_array)
    {

        return base64_encode(json_encode($msg_array));

    }
}