<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GameStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:status {action}';
    //protected $signature = 'swoole:send {user}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'game:status';
    
    
    protected $pid_file;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arg = $this->argument('action');
        switch ($arg) {
            case 'close':
                $res = DB::table('game_status')->where(array('status'=>1))->get()->toArray();
                if($res){
                    Log::debug("close現在時間:".Carbon::now());
                    if(strtotime($res[0]->closes_at) < strtotime(Carbon::now())){
                        Log::debug("gameWebsocketClose開始時間:".$res[0]->closes_at);
                        if(strtotime($res[0]->closee_at) >= strtotime(Carbon::now())){
                            Log::debug("gameWebsocketClose結束時間:".$res[0]->closee_at);
                            $this->call('swoole', [
                                'action' => 'gameWebsocketClose'
                            ], function($console) {
                                var_dump($console);
                            } );
                        }
                    }
                }
            break;
            case 'stop':
                $res = DB::table('game_status')->where(array('status'=>1))->get()->toArray();
                if($res){
                    $id = $res[0]->id;
                    Log::debug("stop現在時間:".Carbon::now());
                    if(strtotime($res[0]->closes_at) < strtotime(Carbon::now())){
                        Log::debug("gameServerClose現在時間:".$res[0]->closes_at);
                        if(strtotime($res[0]->closee_at) >= strtotime(Carbon::now())){
                            if(strtotime($res[0]->closee_at)-60*3 <= strtotime(Carbon::now())){
                                Log::debug("gameServerClose結束時間:".date("Y-m-d H:i:s",strtotime($res[0]->closee_at)-60*2));

                                $test = Artisan::call('game:status', [
                                    'action' => 'clean_tables'
                                ]);
                                Log::debug($test);
                                $this->pid_file =  storage_path('swoole_websocket.pid');
                                if (! $pid = $this->getPid()) {
                                    $this->error("\r\nprocess not started!\r\n");
                                    exit();
                                }
                                try {
                                    \Swoole\Process::kill((int) $pid);
                                    DB::table('game_status')->where(array('id'=>$id))->update(array('status'=>0));
                                    $this->info("\r\nprocess close successful!\r\n");
                                    echo "https://gitlab.com/api/v4/projects/9026235/pipeline?ref=".env("GITLABTAG");
                                    $headers = array();
                                    $headers[] = "PRIVATE-TOKEN: Ex3FKyMCN3yyRCusXqXo";
                                    $headers[] = "Accept: application/json', 'Content-Type: application/json";
                                    $state_ch = curl_init();
                                    curl_setopt($state_ch, CURLOPT_URL,"https://gitlab.com/api/v4/projects/9026235/pipeline?ref=".env("GITLABTAG"));
                                    curl_setopt($state_ch, CURLOPT_HTTPHEADER, $headers);
                                    curl_setopt($state_ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($state_ch,CURLOPT_SSL_VERIFYHOST,0);
                                    curl_setopt($state_ch,CURLOPT_SSL_VERIFYPEER,0);
                                    curl_setopt($state_ch, CURLOPT_POST, true);
                                    
                                    $server_output = curl_exec($state_ch);
                                    
                                    curl_close ($state_ch);
                                    print_r($server_output);
                                    exit();
                                } catch (ErrorException $e) {
                                    $this->info("\r\n".$e->getMessage()."\r\n");
                                }
                                $this->info("\r\nprocess close failed!\r\n");
                            }
                        }
                    }
                }
            break;
            case 'notice':
                $res = DB::table('game_status')->where(array('status'=>1))->get()->toArray();
                if($res){
                    Log::debug("notice現在時間:".Carbon::now());
                    if(strtotime($res[0]->notes_at) < strtotime(Carbon::now())){
                        Log::debug("gameMaintainNotice現在時間:".$res[0]->notes_at);
                        
                        if(strtotime($res[0]->notee_at) >= strtotime(Carbon::now())){
                            Log::debug("gameMaintainNotice結束時間:".$res[0]->notee_at);
                            $this->call('swoole', [
                                'action' => 'gameMaintainNotice'
                            ], function($console) {
                                var_dump($console);
                            } );
                        }
                    }
                }
            break;
            case 'clean_tables':
                echo "清桌嚕";
                DB::table('tables')->update(array('user_id'=>NULL,'user_name'=>NULL,"user_token"=>NULL,"account_name"=>NULL,'status'=>2));
            break;
            default:
                echo "沒事幹";
            break;
        }
    }
    //获取pid
    private function getPid()
    {
        return file_exists($this->pid_file) ? file_get_contents($this->pid_file) :false;
    }
}