<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
class EchoWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'echo:winner';
    //protected $signature = 'swoole:send {user}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'echo:winner';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = array();
        $res = DB::table('result_winner')->whereIn('status', [0])->get()->toArray();
        if($res){
            foreach ($res as $k=>$v){
                if($v->table_id>0 && $v->game_status>0){
                    $temp = array(
                        'user_name'=>$v->user_name,
                        'table_id'=>$v->table_id,
                        'type'=>$v->game_status,
                        'win_amount'=>$v->win_amount,
                    );
                    $data[$k]=$temp;
                    DB::table('result_winner')->where(array('id'=>$v->id))->update(array('status'=>1));
                }
            }
            \Ratchet\Client\connect('ws://'.'127.0.0.1'.':'.env('WEBSOCKET_PORT'))->then(function($conn) use (&$data){
                $conn->on('message', function($msg) use ($conn) {
                    echo "Received: {$msg}\n";
                    $conn->close();
                });
                    $out =array('cmd'=>env('APP_KEY')."_Winner",'data'=>$data);
                    $out = base64_encode(json_encode($out));
                    $conn->send($out);
            }, function ($e) {
                echo "Could not connect: {$e->getMessage()}\n";
            });
        }
    }
}