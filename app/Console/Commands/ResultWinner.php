<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
class ResultWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'result:winner {date}';
    //protected $signature = 'swoole:send {user}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'result:winner';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date');
        $res = DB::table('pk5s')->whereIn('status', [7,8,9,10])->whereDate('created_at', '=', $date)->get()->toArray();
        if($res){
            foreach($res as $key=>$value){
                $res2 = DB::table('users_mapping_games')->where(array('game_id'=>$value->parent_id))->get()->toArray();
                if($res2){
                    $input = array(
                        'table_id'=>isset($res2[0]->table_id)?$res2[0]->table_id:1,
                        'user_id' =>$res2[0]->user_id,
                        'user_name' =>$res2[0]->user_name,
                        'game_status' =>$value->status,
                    );
                    try {
                        DB::table('result_winner')->where(array('game_id'=>$value->parent_id))->update($input);
                    } catch (\Illuminate\Database\QueryException $e) {
                        // something went wrong with the transaction, rollback
                    } catch (\Exception $e) {
                        // something went wrong elsewhere, handle gracefully
                    }
                }
                //exit;
            }
        }
    }
}