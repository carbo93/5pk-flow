<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use ErrorException;

class SwooleManger extends Command
{
    
    private $server;
    
    private $pid_file;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:server {action}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'start or stop the swoole process';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pid_file =  storage_path('swoole_websocket.pid');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //获取传递的操作
        $arg = $this->argument('action');
        switch ($arg){
            case 'gameMaintainNotice':
                $this->gameMaintainNotice();
                break;
            case 'gameWebsocketClose':
                $this->gameWebsocketClose();
                break;
            case 'start':
                //检测进程是否已开启
                $pid = $this->getPid();
                if($pid && \Swoole\Process::kill($pid,0))
                {
                    $this->error("\r\nprocess already exist!\r\n");
                    exit;
                }
                $this->server = new \swoole_websocket_server(env('WEBSOCKET_IP'), env('WEBSOCKET_PORT'));
                $this->server->set([
                    'worker_num'     => env('WORKER_NUM'),
                    'ipc_mode'       => env('IPC_MODE'),
                    'max_request'    => env('MAX_REQUEST'),
                    'max_connection' => env('MAX_CONNECTION'),
                    'dispatch_mode'  => env('DISPATCH_MODE'),
                    'daemonize'      => env('DAEMONIZE'),      //設置程序進入後台作為守護進程運行
                    'heartbeat_check_interval' => env('HEARTBEAT_CHECK_INTERVAL'),
                    'heartbeat_idle_time' => env('HEARTBEAT_IDLE_TIME'),
                    'timeout' => env("TIMEOUT"), 
/*                     'worker_num'=>8,
                    'daemonize' =>1,
                    'max_request'=>1000,
                    'dispatch_mode'=>2, */
                    'pid_file' =>$this->pid_file,
                ]);
                $app = App::make('App\Handles\SwooleWebSocketHandle');
                $this->server->on('open',array($app,'onOpen'));
                $this->server->on('message',array($app,'onMessage'));
                $this->server->on('close',array($app,'onClose'));
                $this->info("\r\nprocess created successful!\r\n");
                
                $this->server->start();
                
                
                break;
                
            case 'stop':
                if (! $pid = $this->getPid()) {
                    $this->error("\r\nprocess not started!\r\n");
                    exit();
                }
                try {
                    \Swoole\Process::kill((int) $pid);
                    $this->info("\r\nprocess close successful!\r\n");
                    exit();
                } catch (ErrorException $e) {
                    $this->info("\r\n".$e->getMessage()."\r\n");
                }
                $this->info("\r\nprocess close failed!\r\n");
                break;
            default:
        }
    }
    //获取pid
    private function getPid()
    {
        return file_exists($this->pid_file) ? file_get_contents($this->pid_file) :false;
    }
    private function gameMaintainNotice()
    {
        \Ratchet\Client\connect('ws://'.'127.0.0.1'.':'.env('WEBSOCKET_PORT'))->then(function($conn) {
            $conn->on('message', function($msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });
                $temp =array('cmd'=>env('APP_KEY')."_Notice");
                $temp = base64_encode(json_encode($temp));
                $conn->send($temp);
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
    }
    
    private function gameWebsocketClose(){
        \Ratchet\Client\connect('ws://'.'127.0.0.1'.':'.env('WEBSOCKET_PORT'))->then(function($conn) {
            $conn->on('message', function($msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });
                $temp =array('cmd'=>env('APP_KEY')."_Close");
                $temp = base64_encode(json_encode($temp));
                $conn->send($temp);
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
            
    }
}
