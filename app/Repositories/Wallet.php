<?php 
namespace App\Repositories;

use App\Repositories\Curl;
use Log;
class Wallet
{

    protected $ulg_api;
    protected $Curl;

    public function __construct(Curl $curl)
    {
        $this->Curl = $curl;
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function walletTransferAck($data)
    { 
        
        $url = $this->ulg_api . "/v1/apis/ulg168/exchange";
        //echo $url;
        $return_data = $this->Curl->post($url, $data);
        Log::debug('walletTransferAck-Input:'.json_encode($data)."@walletTransferAck-ouput:".$return_data);
       
        $msg_array = array(
            'cmd' => 8001021,
            'data' => json_decode($return_data, true),
        );
       // var_dump($return_data);
        return $msg_array;

    }

    public function walletBalaceAck($data){

        $url = $this->ulg_api . "/v1/apis/ulg168/wallet/balance?";
        $url .= "token=".$data["login_token"];
        $url .= "&game_token=".$data["game_token"];
        $url .= "&game_id=".$data["game_id"];

        //echo $url;
        $return_data = $this->Curl->get($url);

        Log::debug('walletBalaceAck-Input:'.json_encode($data)."@walletBalaceAck-ouput:".$return_data);
        $return_array = json_decode($return_data , true);


        //var_dump($return_array);
        $msg_array = array(
            'cmd' => 8001022,
            'data' => array(
                "balance"=>$return_array["balance"],
            )
        );

        return $msg_array;
    }


    public function walletCheckOut($data){

        $url = $this->ulg_api . "/v1/apis/ulg168/checkout";

        $return_data = $this->Curl->post($url, $data);

        Log::debug('walletCheckOut-Input:'.json_encode($data)."@walletCheckOut-ouput:".$return_data);

        return json_decode($return_data, true);

    }

} 
