<?php 
namespace App\Repositories;

use App\Repositories\Curl;
use App\Repositories\Wallet;
use Log;
class GameLogin
{

    protected $ulg_api;
    protected $Curl;

    public function __construct(Curl $curl, Wallet $wallet)
    {
        $this->Curl = $curl;
        $this->Wallet = $wallet;
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function gameLoginAck($data)
    { 

        $url = $this->ulg_api . "/v1/apis/ulg168/auth";
       
        $return_data = $this->Curl->post($url, $data);
        Log::debug('gameLoginAck-Input:'.json_encode($data)."@gameLoginAck-ouput:".$return_data);
        //var_dump($return_data);
        $msg_array = array(
            'cmd' => 8001001,
            'data' => json_decode($return_data, true),
        );

        return $msg_array;

    }

    public function gameLogoutAck($data)
    {

        $return_data = $this->Wallet->walletCheckOut($data);

        $msg_array = array(
            'cmd' => 8001014,
            'data' => $return_data,
        );

        return $msg_array;

    }

    public function loginCheck($data){

        $url = $this->ulg_api . "/v1/apis/ulg168/auth/validate";
        //var_dump($data);
        $return_data = $this->Curl->post($url, $data);
        Log::debug('loginCheck-Input:'.json_encode($data)."@loginCheck-ouput:".$return_data);

        return json_decode($return_data,true);
        
    }

} 
