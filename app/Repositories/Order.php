<?php 
namespace App\Repositories;

use App\Repositories\Curl;
use Log;
class Order
{
    protected $ulg_api;
    protected $order_id;
    protected $Curl;

    public function __construct(Curl $curl)
    {
        $this->Curl = $curl;
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function createOrder($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order";

        $return_data = $this->Curl->post($url, $data);
        Log::debug('createOrder-Input:'.json_encode($data)."@createOrder-ouput:".$return_data);
       
        return $this->json_decode($return_data);

    }

    public function createOrderItem($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_id"] . "/item";

        $return_data = $this->Curl->post($url, $data);

        Log::debug('createOrderItem-Input:'.json_encode($data)."@createOrderItem-ouput:".$return_data);

        return $this->json_decode($return_data);

    }

    public function payoutOrder($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_id"] . "/payout";

        $return_data = $this->Curl->put($url, $data);

        Log::debug('payoutOrder-Input:'.json_encode($data)."@payoutOrder-ouput:".$return_data);

        return $this->json_decode($return_data);

    }

    public function payoutOrderItem($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_id"] . "/item/" . $data["item_id"] . "/payout";
        //echo "PayoutOrderItem";
        $input_data = array(
            "token" => $data["token"],
            "game_id" => $data["game_id"],
            "game_token" => $data["game_token"],
            "rate" => $data["rate"],
            "win" => $data["win"],
            "remark" => $data["remark"],
            "payout_at" => $data["payout_at"],
            "result" => $data["result"]
        );
        $return_data = $this->Curl->put($url, $input_data);

        Log::debug('payoutOrderItem-Input:'.json_encode($input_data)."@payoutOrderItem-ouput:".$return_data);
        
        return $this->json_decode($return_data);

    }

    private function json_decode($msg_array)
    {

        return json_decode($msg_array, true);

    }


} 
