<?php 
namespace App\Repositories;

use App\Repositories\Curl;
use App\Repositories\Table;
use Log;
class Game5pk
{

    protected $game_id;
    protected $sub_game_id = 0;
    protected $Curl;
    protected $Table;

    public function __construct(Curl $curl, Table $table)
    {
        $this->Curl = $curl;
        $this->Table = $table;
    }
    public function pleaseBet($time)
    {

        $msg_array = array(
            'cmd' => 8001006,
            'data' => array(
                'time' => intval($time)
            )
        ); 
   
        return $msg_array;

    }
    /*
    public function pleaseKeepCard($time)
    {

        $msg_array = array(
            'cmd' => 8001008,
            'data' => array(
                "time" => intval($time)
            )
        );
        return $msg_array;

    }
    
    public function pleaseSubGame($time)
    {

        $msg_array = array(
            'cmd' => 8001010,   
            'data' => array(
                "time" => intval($time)
            )
        );
        return $msg_array;

    }
    
    public function gameLoginAck()
    {

        $msg_array = array(
            'cmd' => 8001001,
            'data' => 'success'
        );
        return $msg_array;
    }
*/
    public function betAck()
    {

        $msg_array = array(
            'cmd' => 8001002,
            'data' => array(
                "code" => 1,           //會再定義code的代碼對應
                'success' => true
            )
        ); 

        return $msg_array;

    }

    public function betAckError()
    {

        $msg_array = array(
            'cmd' => 8001002,
            'data' => array(
                "code" => 2,           //會再定義code的代碼對應
                'success' => false
            )
        );

        return $msg_array;

    }

    public function deal($user_id, $user_name, $round, $table_id)
    {
        //if($user_id == "691"){
        //    $cards_array = $this->getWinnerCard();
        //}else{
            $cards_array = $this->getCard();
        //}
        if ($cards_array) {
            $this->game_id = $cards_array["game_id"];

            $this->Table->inGame($user_id, $user_name, $this->game_id , "5pk-1", $round, $table_id);

            $msg_array = array(
                'cmd' => 8001007,   //Deal
                'data' => array(
                    "cards" => $cards_array["cards"]
                )
            );

            return $msg_array;

        } else {
            return false;
        }
    }
    public function keepCardAck($keep_cards_positions_array)
    {

        $cards_array = $this->getKeepCard($this->game_id, $keep_cards_positions_array);
        if ($cards_array) {

            $msg_array = array(
                'cmd' => 8001003,
                'data' => array(
                    "code" => 1,           //會再定義code的代碼對應
                    'success' => true
                )
            );

            return $msg_array;

        } else {

            $msg_array = array(
                'cmd' => 8001003,
                'data' => array(
                    "code" => 2,           //會再定義code的代碼對應
                    'success' => false
                )
            );

            return $msg_array;

        }
    }

    public function gameResult($bet_amount, $win_amount_max)
    {
        
        $cards_array = $this->getFinalCard($this->game_id);
        $win_amount = $bet_amount * $cards_array['rate'];

        if(floatval($win_amount)<floatval($win_amount_max)){
            $continue = 1;
        }else{
            $continue = 0;
        }
        //輸
        if($cards_array['judge_id']==0){
            $continue = 0;
        }

        if ($cards_array) {
            $msg_array = array(
                'cmd' => 8001009,
                'data' => array(
                    "cards" => $cards_array['cards'],
                    "type" => intval($cards_array['judge_id']),
                    "name" => $cards_array['judge_name'],
                    "rate" => floatval($cards_array['rate']),
                    "bet_amount" => floatval($bet_amount),
                    "win_amount" => floatval($win_amount),
                    "continue" => $continue
                )
            );

            //更新大獎中獎金額
            if($cards_array['judge_id']>=7){
                $url = env('LOGIC_5PK_URL') . "/api/winner/mappingGame?game_id=".$this->game_id."&bet_amount=".floatval($bet_amount)."&win_amount=".floatval($win_amount);
                $result = $this->Curl->get($url);
             }
            return $msg_array;
        } else {
            return false;
        }

    }

    public function joinSubGameAck($user_id, $user_name, $round, $table_id)
    {
        $card = $this->getSubGameFirstCard();
        if(isset($card["sub_game_id"])){
            $game_id = $card["sub_game_id"];

            $this->Table->inGame($user_id ,$user_name, $this->game_id , "5pk-2", $round, $table_id);
            $msg_array = array(
                'cmd' => 8001004, 
                'data' => array(
                    'code' => 1,
                    'card' => $card["pc_card"]
                ),
            );
        }else{
            $msg_array = array(
                'cmd' => 8001004, 
                'data' => array(
                    'code' => 2,
                    'card' => 0
                ),
            );
        }
        return $msg_array;
        
    }


    public function selectSubGameCardAck()
    {
        $msg_array = array(
            'cmd' => 8001005,
            'data' => array(
                'code' => 1,
                'success' => true
            ),
        );
        return $msg_array;
    }

    public function broadcast($msg_array)
    {
        $msg_array = array(
            'cmd' => 8001013,
            'data' => array(
                'table' => $msg_array["table"],
                'user_name' => $msg_array["user_name"],
                'type' => $msg_array["type"],
                'win_amount'=>$msg_array["win_amount"],
            ),
        );
        return $msg_array;
    }

    public function maintainNotice($time)
    {
        $msg_array = array(
            'cmd' => 8001016,
            'data' => array(
                'time' => $time,
            ),
        );
        return $msg_array;
    }
    public function maintainClose()
    {
        $msg_array = array(
            'cmd' => 8001020,
        );
        return $msg_array;
    }

    public function subGameResult($position, $bet_amount, $win_amount_max)
    {
        
        //贏兩倍 $cards_array["status"] = 3
        //輸結算 $cards_array["status"] = 1
        //平手下一局 $cards_array["status"] = 2\
        //echo "subGameResult";
        $cards_array = $this->getSubGameFinalCard($position);
        //var_dump($cards_array);
        switch ($cards_array["status"]) {
            case 1:
                $rate = 0;
                break;
            case 2:
                $rate = 1;
                break;
            case 3:
                $rate = 2;
                break;
        }
       

        
        $bet_amount = floatval($bet_amount);
        $win_amount = floatval($bet_amount * $rate);

        if(floatval($win_amount)<floatval($win_amount_max)){
            $continue = 1;
        }else{
            $continue = 0;
        }

        $msg_array = array(
            'cmd' => 8001011,
            'data' => array(
                'cards' => $cards_array["cards"],
                'type' => $cards_array["status"],
                'rate' => $rate,
                'bet_amount' => $bet_amount,
                'win_amount' => $win_amount,
                'continue'=> $continue
            )
        );
        return $msg_array;

    }

    public function settlement($final_amount)
    {

        //$this->sub_game_id = 0;
        $msg_array = array(
            'cmd' => 8001012,
            'data' => array(
                'balance' => floatval($final_amount)
            )
        );

        return $msg_array;

    }

    public function errorMessage($error_code)
    {

        $msg_array = array(
            'cmd' => 8001015,
            'data' => array(
                'code' => $error_code,
            )
        );

        return $msg_array;

    }

    private function getCard()
    {
        
        $url = env('LOGIC_5PK_URL') . "/api/5pk/sp1";
        $result = $this->Curl->get($url);
        $card_array = json_decode($result, true);
        Log::debug('getCard_result:'.$result);

        if(isset($card_array['data']['A'])){
            if ($card_array['status']) {

                $cards = $card_array['data']['A'];
                $game_id = $card_array['data']['id'];
                $this->game_id = $game_id;
                $return_data = array(
                    "game_id" => $game_id,
                    "cards" => $cards
                );
                return $return_data;

            } else {

                return false;

            }
        }else{
            return false;
        }
    }

    private function getWinnerCard()
    {

        $url = env('LOGIC_5PK_URL') . '/api/test/sp1';
        $card_array = json_decode($this->Curl->get($url), true);
        if(isset($card_array['data']['A'])){
            if ($card_array['status']) {

                $cards = $card_array['data']['A'];
                $game_id = $card_array['data']['id'];
                $this->game_id = $game_id;
                $return_data = array(
                    "game_id" => $game_id,
                    "cards" => $cards
                );
                return $return_data;

            } else {

                return false;

            }
        }else{
            return false;
        }
    }


    private function getKeepCard($game_id, $positions_array)
    {
        $positions_str = "";
        for ($i = 0; $i < count($positions_array); $i++) {

            $positions_str .= (string)$positions_array[$i];
            if ($i < count($positions_array) - 1) {
                $positions_str .= ",";
            }

        }

        $url = env('LOGIC_5PK_URL') . "/api/5pk/sp1?A=" . $positions_str . "&id=" . $game_id;
        $result = $this->Curl->get($url);
        Log::debug('getKeepCard_URL:'.$url.'@getKeepCard_result:'.$result);
        $card_array = json_decode($result, true);

        if(isset($card_array['status'])){
            if($card_array['status']) {
                $cards = $card_array['data']['A'];
                $game_id = $card_array['data']['id'];
                $return_data = array(
                    "game_id" => $game_id,
                    "cards" => $cards
                );
                return $return_data;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    private function getFinalCard($game_id)
    {

        $url = env('LOGIC_5PK_URL') . "/api/5pk/sp1";
        $data = array(
            "id" => $game_id
        );
        //Log::debug('getFinalCard_game_id:'.$game_id);
        $result = $this->Curl->post($url, $data);
        Log::debug('getFinalCard_game_id:'.$game_id.'@getFinalCard_result:'.$result);
        $card_array = json_decode($result, true);
       
        if(isset($card_array['data']['A'])){
            $cards = $card_array['data']['A'];
            $type = $card_array['data']['judge'];
            $rate = $card_array['data']['rate'];
            $judge_name = $card_array['data']['judge_name'];

            if ($card_array['status']) {

                $return_data = array(
                    "game_id" => $game_id,
                    "cards" => $cards,
                    "judge_id" => $type,
                    "judge_name" => $judge_name,
                    "rate" => $rate,
                );
                return $return_data;

            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    private function getSubGameFirstCard()
    {
        if ($this->sub_game_id == 0) {
            $url = env('LOGIC_5PK_URL') . "/api/5pk/sp2";
        } else {
            $url = env('LOGIC_5PK_URL') . "/api/5pk/sp2?id=" . $this->sub_game_id;
        }

        $result = $this->Curl->get($url); 
        Log::debug('getSubGameFirstCard_URL:'.$url.'@getSubGameFirstCard_result:'.$result);
        $card_array = json_decode($result, true);
       
        if(isset($card_array['data']['pc'])){
            $pc_card = $card_array['data']['pc'];
            $sub_game_id = $card_array['data']['id'];
            $this->sub_game_id = $sub_game_id;
            if ($card_array['status']) {

                $return_data = array(
                    "sub_game_id" => $sub_game_id,
                    "pc_card" => $pc_card,
                );

                return $return_data;

            } else {

                return false;
            }
        }else{
            return false;
        }
    }

    private function getSubGameFinalCard($select_card_position)
    {
        //echo "getSubGameFinalCard";
        $url = env('LOGIC_5PK_URL') . "/api/5pk/sp2";
        //echo $url;
        $data = array(
            "id" => $this->sub_game_id,
            "site" => $select_card_position
        );

        $result = $this->Curl->post($url, $data);
        Log::debug('getSubGameFinalCard_URL:'.$url."->".json_encode($data).'@getSubGameFinalCard_result:'.$result);
        $card_array = json_decode($result, true);
        if(isset($card_array['data']['cards'])){
            $cards = $card_array['data']['cards'];
            $status = $card_array['data']['status'];
            //$status = 1  輸   2 平手 3 贏

            if ($card_array['status']) {

                $return_data = array(
                    "cards" => $cards,
                    "status" => $status,
                );

                return $return_data;

            } else {

                return false;

            }
        }else{
            return false;
        }

    }

} 