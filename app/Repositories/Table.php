<?php 
namespace App\Repositories;
use Log;
class Table { 
    protected $url = "";
    protected $Curl;
    function __construct(Curl $curl){
        $this->Curl = $curl;
    }
    public function showTable(){
        $url = env("LOGIC_5PK_URL")."/api/table/list";
        //echo $createTableUrl;
        //exit;
        //?room_id=1&token=123123123"
        $res = $this->Curl->get($url);

        Log::debug('showTable-Input:'.$url."@showTable-ouput:".$res);

        $res = json_decode($res,true);   

        //tableListAck
        $msg_array = array(
            'cmd' => 8001018,
            'data' => $res
        );
        return $msg_array;
    }
    public function inTable($inputArray){

        $createTableUrl = env("LOGIC_5PK_URL")."/api/table/create";
        $res = $this->Curl->post($createTableUrl, $inputArray);
        Log::debug('inTable-Input:'.json_encode($inputArray)."@inTable-ouput:".$res);
        $res = json_decode($res,true);

        //joinTableAck
        $msg_array = array(
            'cmd' => 8001017,
            'data' => array(
                "status" => $res['status']
            )
        ); 
        return $msg_array;
    }
    public function leaveTable($inputArray){
        //echo "leaveTable";
        //var_dump($inputArray);
        $Url = env("LOGIC_5PK_URL")."/api/table/leave";
        //var_dump($Url);
        
        $res = $this->Curl->post($Url, $inputArray);
        Log::debug('leaveTable-Input:'.json_encode($inputArray)."@leaveTable-ouput:".$res);
        ////var_dump($res);
        $res = json_decode($res,true);
        //leaveTableAck
        $msg_array = array(
            'cmd' => 8001019,
            'data' => array(
                "status" => $res['status']
            )
        );
        return $msg_array;
    }

    public function inGame($user_id ,$user_name, $game_id ,$game_type, $round, $table_id){

        //echo "ingame";
        try{
            $createTableUrl = env("LOGIC_5PK_URL")."/api/table/mappingGame";
            $input = array('user_id'=>$user_id, 'user_name'=>$user_name, 'game_id'=>$game_id, 'game_type' => $game_type,'level'=>$round, 'table_id'=>$table_id);
            $res = $this->Curl->post($createTableUrl, $input);
            Log::debug('inGame-Input:'.json_encode($input)."@inGame-ouput:".$res);
            if($res){
                $res = json_decode($res,true);
                if($res['status']){
                    return true;
                }
            }
            return false;
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }
}