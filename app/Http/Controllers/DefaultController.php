<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class DefaultController extends BaseController
{

    private $_javascript = array();

    private $_css = array();
    
    protected $assign = array();

    function __construct()
    {
        
    }

    protected function css()
    {
        $css_files = func_get_args();
        
        foreach ($css_files as $css_file) {
            //$css_file = substr($css_file, 0, 1) == '/' ? substr($css_file, 1) : $css_file;
            
            $is_external = false;
            if (is_bool($css_file))
                continue;
            
            $is_external = preg_match("/^https?:\/\//", trim($css_file)) > 0 ? true : false;
            
            if (! $is_external)
                if (! file_exists(public_path().$css_file))
                    print_R("Cannot locate stylesheet file: {$css_file}.");
            
            $css_file = $is_external == FALSE ? base_url() . $css_file : $css_file;
            
            if (! in_array($css_file, $this->_css))
                $this->_css[] = $css_file;
        }
        return;
    }

    protected function js()
    {
        $script_files = func_get_args();
        
        foreach ($script_files as $script_file) {
            //$script_file = substr($script_file, 0, 1) == '/' ? substr($script_file, 1) : $script_file;
            
            $is_external = false;
            if (is_bool($script_file))
                continue;
            
            $is_external = preg_match("/^https?:\/\//", trim($script_file)) > 0 ? true : false;
            if (! $is_external){
                if (! file_exists(public_path().$script_file))
                    print_R("Cannot locate javascript file: {$script_file}.");
            }
            $script_file = $is_external == FALSE ? base_url() . $script_file : $script_file;
            
            if (! in_array($script_file, $this->_javascript))
                $this->_javascript[] = $script_file;
        }
        
        return;
    }

    protected function get_js_files(){
        return $this->_javascript;
    }
    protected function get_css_files(){
        return $this->_css;
    }
    public function image(Request $request){
        $interval = 120 ; 
        $get = $request->all();
        $segments = $request->segments();
        if(isset($segments[0]) && $segments[0]=="img"){
            unset($segments[0]);
            $file_name = storage_path('upload/images/'.implode("/", $segments));
            //echo $file_name;
            //var_dump(is_file($file_name));
            if(is_file($file_name)){
                if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
                    $c_time = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) + $interval;
                    if($c_time > time()){
                        header('HTTP/1.1 304 Not Modified');
                        exit();
                    }
                }
                $result = array();
                $result['filesize']      = filesize($file_name);
                $result['content']       = file_get_contents($file_name);
                $result['filemtime']     = filemtime($file_name);
                $result['info']          = GetImageSize($file_name);
                header('Cache-Control:max-age='.$interval);
                header("Expires: " . gmdate("D, d M Y H:i:s",time()+$interval)." GMT");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Content-Length: ".$result['filesize']);
                header("Content-Type: {$result['info']['mime']}");
                echo $result['content'];
            }else{
                header("HTTP/1.0 404 Not Found");
                exit;
            }
            
        }else{
            header("HTTP/1.0 404 Not Found");
            exit;
        }
    }
}