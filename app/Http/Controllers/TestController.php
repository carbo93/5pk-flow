<?php 
namespace App\Http\Controllers;

use App\Repositories\Room;

class TestController extends Controller
{
    /**
     */
    protected $room;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(Room $room)
    {
        $this->room = $room;
    }
    public function index(){
        $room_id = 1 ; 
        $token = "test";
        $table_id =  $this->room->inTable($room_id, $token);
        $game_id = 1;
        $game_type ="5pk-1";
        $level ="1";
        echo $this->room->inGame($table_id, $game_id, $game_type, $level);
    }
}